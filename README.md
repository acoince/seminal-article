## About

Author support service provides LaTeX style files and `.tex` file templates designed for *IOS Press* journal articles.

## Contents

The following files are given in the repository (or directly in `.zip` archive):

- `iosart2x.cls`, `iosart2x.cfg` - LaTeX style files designed for *IOS Press* journal articles. Please do not change them.
- `ios1.bst`, `bibliography.bib` - BibTeX related files. If your bibliography is structured in BibTeX format, loading your `*.bib` file and provided BibTeX style `ios1.bst` allows you to get the final format of the bibliography. Please note that `bibtex` program should be used to generate the `*.bbl` file. `bibliography.bib` is the minimal sample of `*.bib` file.

## Setup

- Clone the repository or download the `.zip` archive;
- Install LaTeX style files (`iosart2x.cls`, `iosart2x.cfg`) in your TeX system;
- Run `latexmk -pdf main.tex` to generate the manuscript.

