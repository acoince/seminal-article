% add. options: [seceqn,secthm,crcready,onecolumn]
\documentclass[sw]{iosart2x}

%\usepackage{dcolumn}
%\usepackage{endnotes}
\usepackage[printonlyused]{acronym}

%%%%%%%%%%% Put your definitions here

\acrodef{OSN}{\emph{Online Social Network}}

%%%%%%%%%%% End of definitions

\pubyear{0000}
\volume{0}
\firstpage{1}
\lastpage{1}

\begin{document}

\begin{frontmatter}

%\pretitle{}
\title{Science beyond repositories with ActivityPub}
\runningtitle{Science beyond repositories with ActivityPub}
%\subtitle{}

% Authors:
\author{\inits{P-A.}\fnms{Pierre-Antoine} \snm{Rault}\ead[label=e1]{pierre-antoine.rault@univ-lorraine.fr}},

\author{\inits{C.}\fnms{Christophe} \snm{Cerisara}\ead[label=e2]{christophe.cerisara@univ-lorraine.fr}}
\address{LORIA - UMR 7503, \orgname{Université de Lorraine}, Nancy, \cny{France} \printead[presep={\\}]{e1,e2}}

%\begin{review}{editor}
%\reviewer{\fnms{First} \snm{Editor}\address{\orgname{University or Company name}, \cny{Country}}}
%\reviewer{\fnms{Second} \snm{Editor}\address{\orgname{First University or Company name}, \cny{Country}
%    and \orgname{Second University or Company name}, \cny{Country}}}
%\end{review}
%\begin{review}{solicited}
%\reviewer{\fnms{First} \snm{Solicited reviewer}\address{\orgname{University or Company name}, \cny{Country}}}
%\reviewer{\snm{anonymous reviewer}}
%\end{review}
%\begin{review}{open}
%\reviewer{\fnms{First} \snm{Open Reviewer}\address{\orgname{University or Company name}, \cny{Country}}}
%\end{review}

\begin{abstract}
Scholarly data is often seen as an immutable asset once published, and stored on institutional or private-sector repositories to ensure long term storage. But before and after a publication, scientists feel the need to reach out and discuss on social networks, leading meta-repositories merging the two like ResearchGate, Mendeley and Academia.edu to gain control of an evergrowing part of the scientific community discourse and exposure. We argue FAIR data principles supporting interoperability of repositories are not enough to build sustainable networks of repositories. Instead, we suggest to extend interoperability via ActivityPub to allow datasets or papers to be commented and shared accross any platform implementing that standard. That opens the way for third-parties or repositories themselves to provide an interface for social interactions without handing over control to a central actor. That also allows scholarly data to be merged back with the discussions they trigger, and even suggest changes based on that input. To that end, we present \textit{OLKi}: a web application with social features \textit{and} a data repository implementing these principles - and \textit{SciFed}, an ontology in the ActivityPub realm. We compare them to existing standards for data repository that provide interoperability.
\end{abstract}

\begin{keyword}
\kwd{Linked Data}
\kwd{Semantic web}
\kwd{ActivityPub}
\kwd{Infrastructure}
\kwd{Data Repository}
\kwd{Online Social Network}
\kwd{Interoperability}
\end{keyword}

\end{frontmatter}

%%%%%%%%%%% The article body starts:

\section{Introduction}
\label{sec:introduction}

% § presenting what data repositories are
Scholarly archives and repositories have proliferated for decades, starting with institutional repositories, of which there are now thousands. They have been gradually unified through numerous portals indexing each other, all thanks to interoperable standards built for the very purpose of having a single, easy search interface. In that respect, standards such as OAI-PMH overwhelmingly succeeded in enabling aggregation of metadata descriptions of records in compatible archives to be harvested by third-parties. These third-parties would then offer value-added services that leveraged the content in the archives – e.g. by offering peer review services and/or overlay journals.

% § explaining what open access variants mean
Before we talk of the problems posed by third-parties, we should take a closer look at the content of repositories: it can be journal papers (often papers yet to be published -- preprints have been the focus of major repositories like arXiv since 1991), or generally any corpus of research data. Once deposited on a data repository for archival, their metadata are openly accessible. Most of the controversy with third-parties and especially private-sector third-parties revolves around journal papers, but what can be said about them applies to any data for the most part too.

% § detailling the prevalence of social networks & why we should care about social networks for scientists and social networks in general. (spoiler: you can't entrust a central actor with your scientific workflow, else they might seek to make profit a your expense)
Fast forward to 2019, third-parties have become the unavoidable gateway to scientific content, and their services are now enshrined in the habits of a researcher: cross-referencing, citing, sharing to peers, and social interaction among other things. Legacy publishers have also joined the dance since their subscription revenues are expected to decline as open access grows, and thus felt under enough pressure to create new markets around open access. It led to controversial acquisitons like that of central repository sites like Mendeley and SSRN by Elsevier, where the publishers are put in control of content beyond the reach of peer review: social activities. In the social era, the value is not anymore in the content of a paper, but also in the interactions with it -- and publishers have started to take control of it.
Worse, some institutions are directly delegating archival services to publishers' platforms \footnote{\url{https://purl.org/olki/elsevier-uf-partnership}} for convenience and compliance \footnote{\url{https://purl.org/olki/institutional-compliance}}.
What was once seen as added value is becoming the requirement for some institutions, and the future of open access is increasingly claimed by publishers, which want to own as much infrastructure of this future as possible.

% § detailing their role in "gold open access" (see Q&A with CNI’s Clifford Lynch) as opposed to "green open access" (see https://en.wikipedia.org/wiki/Self-archiving)
While we want to stress no amount of technology will replace institutional will and funding to improve their tools and reach compliance, we can certainly help to build a more featureful and independent backbone for the future of scientific data and the interactions around it. To that end, we propose an ontology for interactions around scientific data, using ActivityPub as a base. The latter has already been extensively tested at scale with millions of users across dozens of different implementations in what now constitutes the \textit{fediverse}, a network of social platforms that allow social interactions with and around text messages (in a Twitter-like fashion), videos (resp. for YouTube) and various other content types. By using our own dialect of ActivityPub as a base for what we call the \textit{SciFed} dialect, we stay compatible with textual interactions from the \textit{fediverse}, thus boostraping with millions of users from across the world to review our scientific data, and demonstrating future compatibility with third-party repositories wishing to implement our dialect.

Our implementation is meant as a proof-of-concept that showcases such services, demonstrating they can be deployed easily and run cost-effectively thanks to their low hardware requirements and decentralized architecture.

\section{Related Work}\label{sec:lessons}

OAI-PMH, through its ease of implementation, managed a wide uptake among data repositories, allowing the creation of \textit{portals} meant as aggregators -- often providing services on top of the aggregated data. The implications however are dire: users are attracted to the portals for the added value of their services. These services, on the contrary of the datasets they revolve around, are not exposed in an interoperable way. That refrains users to change for another platform as leaving will prevent them from using those services. While some can be found easily on diverse portals (search, metrics, analysis, etc.), other services are less portable (comments, peer reviews, view counts, etc.) and make its user more captive.

The increased concentration of the scientific community on platforms acting as portals, social networks and authoring platforms is an live example of that network effect and its problems: the initial value of datasets produced by the community gets harvested and displaced to the bigger players that own such platforms. Putting the added value of services back on decentralized nodes like data repositories have done with datasets would allow the scientific community to own their tools, and allow users to move more freely from platform to platform.

Protocols like ActivityPub follow a lineage of social networking protocols dating back from OStatus, effectively mapping social interactions to network operations in order to deliver notifications and content. The use case for which most of these protocols are designed is that of a micro-blogging \ac{OSN}, reminiscent of Twitter. ActivityPub however has proven flexible enough through multiple implementations to replicate even functionalities that are outside the scope of traditional social platforms. Beyond the original use case of \ac{OSN} services, being to model connections to friends and acquaintances, lies richer experiences than just text messages: exchanging events, likes, reviews -- and even machine-generated content like view counts, bibliometrics and server statuses. While often only the most basic features are common to every implementation of any protocol, we argue that ActivityPub is more holistic than other protocols, and the incurred flexibility allows us to map operations more specific to the scientific community.

\section{A proof of concept: OLKi}\label{sec:pow}

We developed the OLKi platform to realize the aforementioned features while primarily focusing on what obstacles are often met for adoption: ease of deployment, and ease of re-implementation. The former is for users that wish to use our platform, and the latter is for developers that wish to be compatible with it. The section \ref{sec:scifed} describing \emph{SciFed} features a guide for implementers.

OLKi is aimed at end users and showcasing some of what is possible. It has more features than what some data repositories might want: only expose their data to the outside world. To that end, section \ref{sec:architecture} describes what a minimal version of OLKi could look like.

\subsection{Operating principles}\label{sec:principles}

Based on these diverse needs, such functionalities include:

\begin{itemize}
	\item \emph{Harvesting}: in the same fashion tha OAI-PMH and OAI-ORE.
	\item \emph{Subscription} avoiding the need to check for new content on a server: upon new content of an actor (server/author on a server).
	\item \emph{Social services} interoperable with other social networks relying on ActivityPub, allowing  widespread interactions such as commenting a corpus, or following an author to stay updated upon content publication.
\end{itemize}

\subsection{Architecture}\label{sec:architecture}

OAI-PMH suffers from the \emph{pull} nature of the protocol, which means polling servers regularly is the only way to present users fresh information. A more modern option - albeit admittedly more complex to implement - is to use a publish–subscribe protocol, whereby one gets notified of new content they have previously subscribed to. Considering most of the scientist go on social networks to follow peers \cite{collins_how_2016}, the need for notifications of need content is pressing.

\subsection{Overview of SciFed}\label{sec:scifed}

% discuss OAI-ORE, ActivityPub, ActivityStreams

\section{Conclusion}\label{sec:conclusion}

% § press that the goal is not to change scholarly communication itself, as we don't plan to solve the gold open access system

\section*{Acknowledgements}\label{sec:acknowledgements}

\footnotesize{This work was supported by the french PIA project “Lorraine Université d’Excellence”, reference ANR-15-IDEX-04-LUE, as part of the Open Language and Knowledge for Citizens (OLKi) project, and developed by Université de Lorraine and its laboratories LORIA, ATILF, IECL, Archives Henri Poincaré and CREM.}
%%%%%%%%%%% The bibliography starts:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                  The Bibliography                       %%
%%                                                         %%
%%  ios1.bst will be used to                               %%
%%  create a .BBL file for submission.                     %%
%%                                                         %%
%%                                                         %%
%%  Note that the displayed Bibliography will not          %%
%%  necessarily be rendered by Latex exactly as specified  %%
%%  in the online Instructions for Authors.                %%
%%                                                         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\nocite{*}
% if your bibliography is in bibtex format, use those commands:
\bibliographystyle{ios1}   % Style BST file.
\bibliography{main}        % Bibliography file (usually '*.bib')

\end{document}
